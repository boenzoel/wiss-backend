<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->post('/', function () use ($router) {
    return $router->app->version();
});

$router->post('/key', function() {
    return \Illuminate\Support\Str::random(32);
});


$router->group(['prefix' => 'api'], function () use ($router) { 

    $router->post('/auth/login', 'AuthController@authenticate');

    $router->group(['middleware' => 'jwt.auth'], function () use ($router) {

        $router->post('/users', function() {
            
            $users = \App\Models\User::all();
            return response()->json($users);
        });

        $router->group(['prefix' => 'dashboard'], function () use ($router) {
            //list data
            $router->post('', 'DashboardController@index');
            
        });

        $router->group(['prefix' => 'anggota'], function () use ($router) {
            //list data
            $router->post('', 'Master\AnggotaController@index');

            //view data
            $router->post('/show', 'Master\AnggotaController@view');

            //save data
            $router->post('', 'Master\AnggotaController@post');

            //update data
            $router->put('', 'Master\AnggotaController@put');

            //update selected data
            $router->patch('', 'Master\AnggotaController@patch');

            //delete data
            $router->delete('', 'Master\AnggotaController@delete');
            // $router->options($uri, $callback);
        });

    });

});


//temp 
$router->post('/auth/login', 'AuthController@authenticate');

$router->post('/users', function() {
    $users = \App\Models\User::all();
    return response()->json($users);
});


$router->group(['middleware' => 'jwt.auth'], function () use ($router) {

        $router->group(['prefix' => 'dashboard'], function () use ($router) {
            //list data
            $router->get('', 'DashboardController@index');
            
        });

        $router->group(['prefix' => 'anggota'], function () use ($router) {
            //list data
            $router->get('', 'Master\AnggotaController@index');

            //view data
            $router->get('/show', 'Master\AnggotaController@view');

            //save data
            $router->post('', 'Master\AnggotaController@post');

            //update data
            $router->put('', 'Master\AnggotaController@put');

            //update selected data
            $router->patch('', 'Master\AnggotaController@patch');

            //delete data
            $router->delete('', 'Master\AnggotaController@delete');
            // $router->options($uri, $callback);
        });

});