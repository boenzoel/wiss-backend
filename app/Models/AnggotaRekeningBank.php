<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Anggota;

class AnggotaRekeningBank extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'TAnggota_RekBank';

    protected $connection = 'koperasi';

    public static function getTableName()
    {
        return 'TAnggota_RekBank';
    }

    public function anggota()
    {
        return $this->morpMany(Anggota::class,'rekeningBank');
    }
}