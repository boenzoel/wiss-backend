<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\AnggotaRekeningBank;

class Anggota extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'TAnggota';

    protected $connection = 'koperasi';

    protected $primaryKey = 'kdAnggota';

    public function rekeningBank()
    {
        return $this->belongsTo('App\Models\AnggotaRekeningBank','kdAnggota','kdAnggota');
    }
}