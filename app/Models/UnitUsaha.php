<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UnitUsaha extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'TKantor';

    protected $primaryKey = 'KdKantor';

    protected $connection = 'koperasi';

}