<?php

namespace App\Models\Main;

use App\Support\KoperasiConnector;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string tenant_host
 * @property string tenant_database
 * @property string tenant_username
 * @property string tenant_password
 * @property string tenant_name
 */
class Koperasi extends Model {
    
    use KoperasiConnector;
       
    protected $connection = 'main';    
    /**
     * @return $this
     */
    public function connect() {
        $this->reconnect($this);
        return $this;
    }
    
}