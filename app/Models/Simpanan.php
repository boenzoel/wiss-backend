<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Simpanan;

class Simpanan extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'TSaldoSimpanan';

    protected $connection = 'koperasi';



}