<?php

namespace App\Helpers;

use Exception;
use Illuminate\Support\Str;

class Utilities
{
    
    public function UploadFile($param,$uploadModule)
    {
        $image = null;
        $imgFile = null;
        $file_ext = null;
        $destination_path = null;

        try{

            $imgFile = $param;
            if($uploadModule=="profile"){
                $destination_path = storage_path('image/profile');             
            }elseif($uploadModule=="loan"){
                $destination_path = storage_path('document/loan');                
            }elseif($uploadModule=="register"){
                $destination_path = storage_path('document/register');                
            }

            $original_filename = $imgFile->getClientOriginalName();
            $original_filename_arr = explode('.',$original_filename);
            $file_ext = end($original_filename_arr);    
            $strRdm = Str::lower(Str::random(15));     
            $image = $strRdm.'.' .$file_ext;

            if($imgFile->move($destination_path,$image)) {
                return collect([
                    'status' => true,
                    'filename' => $image
                ]);
            }else{
                return collect([
                    'status' => false,
                    'filename' => $image
                ]);
            }
        }catch(Exception $e){

            return $e;

        }
        
    }


    public function UploadMultipleFile($param,$uploadModule)
    {
        $image = null;
        $imgFile = null;
        $file_ext = null;
        $destination_path = null;

        try{

            if($uploadModule=="profile"){
                $destination_path = storage_path('image/profile');             
            }elseif($uploadModule=="loan"){
                $destination_path = storage_path('document/loan');                
            }elseif($uploadModule=="register"){
                $destination_path = storage_path('document/register');                
            }

            foreach($param as $file){

                $imgFile = $file;
                $original_filename = $imgFile->getClientOriginalName();
                $original_filename_arr = explode('.',$original_filename);
                $file_ext = end($original_filename_arr);    
                $strRdm = Str::lower(Str::random(15));     
                $image = $strRdm.'.' .$file_ext;
                if($imgFile->move($destination_path,$image)) {
                    
                    $Uploaded[] = $image;

                }else{

                    return collect([
                        'status' => false,
                        'filename' => $image
                    ]);

                }

            }

            if($Uploaded!=null){
                return collect([
                    'status' => true,
                    'filename' => $Uploaded
                ]);
            }

        }catch(Exception $e){

            return $e;

        }
        
    }

    public function calPMT($apr, $term, $loan)
    {
        $term = $term;
        $apr = $apr / 1200;
        $amount = $apr * -$loan * pow((1 + $apr), $term) / (1 - pow((1 + $apr), $term));
        return round($amount);
    }

    public function generateOTP(){
        $rndno=rand(100000, 999999);//OTP generate
        return $rndno;
    }

}
