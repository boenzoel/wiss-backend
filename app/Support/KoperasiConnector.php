<?php

namespace App\Support;

use App\Models\Main\Koperasi;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

trait KoperasiConnector {

   /**
    * Switch the Tenant connection to a different company.
    * @param Koperasi $company
    * @return void
    * @throws
    */
   public function reconnect(Koperasi $koperasi) {     
      
      // Erase the tenant connection, thus making Laravel get the default values all over again.
      DB::purge('main');
      
      // Make sure to use the database name we want to establish a connection.
      Config::set('database.connections.main.host', $koperasi->host);
      Config::set('database.connections.main.database', $koperasi->database);
      Config::set('database.connections.main.username', $koperasi->username);
      Config::set('database.connections.main.password', $koperasi->password);

      // Rearrange the connection data
      DB::reconnect('main');

      
      // dd(DB::connection('koperasi')->getConfig());

      // Ping the database. This will throw an exception in case the database does not exists or the connection fails
      Schema::connection('main')->getConnection()->reconnect();

      
      
      // dd(DB::connection()->getConfig());
   }
   
}