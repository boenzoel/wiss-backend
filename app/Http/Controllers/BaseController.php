<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Models\Main\Koperasi;
use Illuminate\Http\Request;
use App\Support\KoperasiConnector;
use EllipseSynergie\ApiResponse\Contracts\Response;
use Illuminate\Support\Facades\Crypt;

class BaseController extends Controller
{

    use KoperasiConnector;
    //
    public function failedApi(Response $status,$Errormessage)
    {
        return [
            'success' => [
                'status_code' => $status->getStatusCode(),
                'message' => $Errormessage
            ]
        ];
    }

    public function failed($Errormessage)
    {
        return [
            'success' => [
                'status_code' => 200,
                'message' => $Errormessage
            ]
        ];
    }


    public function success($message)
    {
        return [
            'success' => [
                'status_code' => 200,
                'message' => $message
            ]
        ];
    }


    public function changeConnection(Request $request) {

        $license_code = $request->input('license_code');
        if($request->input('license_code') === null){
            $license_code = $request->header('license_code');
        }

        $data = DB::table('TAccess')->where('license_code', $license_code)->get();

        // dd( $license_code );
        // dd(Crypt::decryptString(trim($data[0]->password)));
        if($data){
        
            if($data->count()>0){
                
                $dataConnection = new Koperasi();
                $dataConnection->host = trim($data[0]->host);
                $dataConnection->database = trim($data[0]->database);
                $dataConnection->username = trim($data[0]->username);
                $dataConnection->password = Crypt::decryptString(trim($data[0]->password));
                $this->reconnect($dataConnection);
            
                // dd($dataConnection);
                return true;
            }
        }

        return false;
     
    }

}