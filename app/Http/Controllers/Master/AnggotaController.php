<?php

namespace App\Http\Controllers\Master;

use App\Repositories\Anggota\AnggotaInterface;
use App\Http\Controllers\Controller;

use DB;
use Illuminate\Support\Facades\Schema;


class AnggotaController extends Controller
{
    /**
     * The user repository instance.
     */
    protected $anggota;

    /**
     * Create a new controller instance.
     *
     * @param  AnggotaRepository  $anggota
     * @return void
     */
    public function __construct(AnggotaInterface $anggota)
    {
        $this->anggota = $anggota;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->anggota->getDataPaginate(10);
        return $data;
    }

}