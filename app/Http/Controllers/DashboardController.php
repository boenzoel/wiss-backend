<?php

namespace App\Http\Controllers;

use App\Repositories\Anggota\AnggotaInterface;
use App\Repositories\Simpanan\SimpananInterface;
use App\Repositories\Pinjaman\PinjamanInterface;
use App\Repositories\UnitUsaha\UnitUsahaInterface;
use App\Http\Controllers\Controller;

use DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Http\Request;


class DashboardController extends Controller
{

    protected $anggota;
    protected $simpanan;
    protected $pinjaman;
    protected $unit_usaha;

    public function __construct(AnggotaInterface $anggota, SimpananInterface $simpanan, PinjamanInterface $pinjaman, UnitUsahaInterface $unit_usaha)
    {
        $this->anggota = $anggota;
        $this->simpanan = $simpanan;
        $this->pinjaman = $pinjaman;
        $this->unit_usaha = $unit_usaha;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req)
    {

        //data anggota
        
        
        

        //data simpanan
        

        

        //data pinjaman
        

       

        

        $data_delinquent = $this->pinjaman->getDataSummaryaDelinquent();

        

        

        

        

        if( $req->target == 'anggota'){
            $status_anggota_summary = $this->anggota->getDataStatusSummary();
            $data = $status_anggota_summary;
        }
        else if( $req->target == 'anggota_register_monthly_summary'){
            $anggota_register_monthly_summary = $this->anggota->getDataSummaryRegisterMonthly();
            $data = $anggota_register_monthly_summary;
        }
        else if( $req->target == 'anggota_register_yearly_summary'){
            $anggota_register_yearly_summary = $this->anggota->getDataSummaryRegisterYearly();
            $data = $anggota_register_yearly_summary;
        }
        else if( $req->target == 'simpanan'){
            $data_simpanan_per_produk = $this->simpanan->getDataStatusSummary();
            $data = $data_simpanan_per_produk;
        }
        else if( $req->target == 'simjaka'){
            $data_simpanan_berjangka = $this->simpanan->getDataSimpananBerjangkaSummary();
            $data = $data_simpanan_berjangka;
        }
        else if( $req->target == 'pinjaman'){
            $data_pinjaman_summary = $this->pinjaman->getDataSumamry();
            $data = $data_pinjaman_summary;
        }
        else if( $req->target == 'pinjaman_monthly_summary'){
            $data_pinjaman_perbulan = $this->pinjaman->getDataSummaryMonthly();
            $data = $data_pinjaman_perbulan;
        }
        else if( $req->target == 'pinjaman_yearly_summary'){
            $data_pinjaman_pertahun = $this->pinjaman->getDataSummaryYearly();
            $data = $data_pinjaman_pertahun;
        }
        else if( $req->target == 'real_pinjaman'){
            $props = ['month'=>$req->month,'year'=>$req->year];
            $data_real_pinjaman = $this->pinjaman->getDataRealPinjaman($props);
            $data = $data_real_pinjaman;
        }
        else if( $req->target == 'laporan_keuangan_pusat_summary'){
            $tahun1 = $req->tahun1;
            $tahun2 = $req->tahun2;
            $data_laporan_keuangan_pusat = $this->pinjaman->getDataLaporanKeuanganPusat($tahun1,$tahun2);
            $data = $data_laporan_keuangan_pusat;
        }
        else if( $req->target == 'laporan_keuangan_unit_usaha_summary'){
            $tahun1 = $req->tahun1;
            $tahun2 = $req->tahun2;
            $unit_usaha = $req->unit_usaha == null ? '02' : $req->unit_usaha;
            $data_laporan_keuangan_unit_usaha = $this->pinjaman->getDataLaporanKeuanganUnitUsaha($tahun1,$tahun2,$unit_usaha);
            $data = $data_laporan_keuangan_unit_usaha;
        }
        else if( $req->target == 'unit_usaha'){
            $data_unit_usaha = $this->unit_usaha->getAll();
            $data = $data_unit_usaha;
        }
        else{
            $data = [];
        }
        //data all
        // $data = [
        //     'anggota_register_yearly_summary' => $anggota_register_yearly_summary,
        //     'anggota_register_monthly_summary' => $anggota_register_monthly_summary,
        //     'status_anggota_summary' => $status_anggota_summary,
        //     'status_simpanan_summary' => $data_simpanan_per_produk,
        //     'simpanan_berjangka_summary' => $data_simpanan_berjangka, 
        //     'pinjaman_summary' => $data_pinjaman_summary,
        //     'pinjaman_monthly_summary' => $data_pinjaman_perbulan,
        //     'pinjaman_yearly_summary'=> $data_pinjaman_pertahun,
        //     'delinquent_summary' => $data_delinquent,
        // ];
        return $data;
        
    }

}