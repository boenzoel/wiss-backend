<?php

namespace App\Http\Controllers;

use Validator;
use App\Models\User;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Firebase\JWT\ExpiredException;
use Illuminate\Support\Facades\Hash;

use DB;

class AuthController extends BaseController 
{
    /**
     * The request instance.
     *
     * @var \Illuminate\Http\Request
     */
    private $request;

    /**
     * Create a new controller instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    public function __construct(Request $request) {
        $this->request = $request;
    }

    /**
     * Create a new token.
     * 
     * @param  \App\User   $user
     * @return string
     */
    protected function jwt(User $user) {
        $payload = [
            'iss' => "lumen-jwt", // Issuer of the token
            'sub' => $user->id, // Subject of the token
            'iat' => time(), // Time when JWT was issued. 
            'exp' => time() + 60*60 // Expiration time
        ];
        
        // As you can see we are passing `JWT_SECRET` as the second parameter that will 
        // be used to decode the token in the future.
        return JWT::encode($payload, env('JWT_SECRET'));
    } 

    /**
     * Authenticate a user and return the token if the provided credentials are correct.
     * 
     * @param  \App\User   $user 
     * @return mixed
     */
    public function authenticate(User $user) {
        $this->validate($this->request, [
            'license_code' => 'required',
            'username'     => 'required',
            'password'  => 'required'
        ]);
        

        $nama_koperasi = '';
        //check license code
        $license_code = $this->request->input('license_code');
        if($license_code !== null){

            //get data koperasi
            $Access = DB::table('TAccess')->where('license_code', $license_code)->get();

            if($Access->count() > 0){
                $KoperasiData = DB::table('TMasterKoperasi')->where('IDKoperasi', $Access[0]->IDKoperasi)->get();

                $nama_koperasi = $KoperasiData[0]->NamaKoperasi;
            }
            //end get data koperasi

            $change = $this->changeConnection($this->request);
            
            if(!$change) {
                return response()->json([
                    'error_code' => 400,
                    'error_type' => 'license_not_valid',
                    'error' => 'License Code is not valid.'
                ], 400);
            }

            
            
        }

        // Find the user by email
        $user = User::where('username', $this->request->input('username'))->first();
       


        if (!$user) {
            // You wil probably have some sort of helpers or whatever
            // to make sure that you have the same response format for
            // differents kind of responses. But let's return the 
            // below respose for now.
            return response()->json([
                'error_code' => 400,
                'error_type' => 'user_not_valid',
                'error' => 'User does not exist.'
            ], 400);
        }

        // return response()->json([
        //     '1' => $this->request->input('password'),
        //     '2' => str_replace(' ', '', $user->password),
        //     '3' => Hash::check($this->request->input('password'), $user->password),
        //     '4' => Hash::make($this->request->input('password'))
        // ], 400);
        // Verify the password and generate the token
        if (Hash::check($this->request->input('password'), str_replace(' ', '', $user->password))) {
            // return response()->json([
            //     'token' => $this->jwt($user)
            // ], 200);

            return $this->respondWithToken($user->username,$nama_koperasi,$this->jwt($user));
        }else{
            // Bad Request response
            return response()->json([
                'error_code' => 400,
                'error_type' => 'password_not_valid',
                'error' => 'password is wrong.'
                // $user->password => Hash::make($this->request->input('password')),
            ], 400);
        }

        // Bad Request response
        return response()->json([
            'error_code' => 400,
            'error_type' => 'all_not_valid',
            'error' => 'Email or password is wrong.'
            // $user->password => Hash::make($this->request->input('password')),
        ], 400);
    }

    // protected function respondWithToken($username,$token)
    // {
    //     return response()->json([
    //         'name' => $username,
    //         'token' => $token,
    //         'token_type' => 'bearer',
    //         // 'expires_in' => Auth::factory()->getTTL() * 60
    //     ], 200);
    // }
}