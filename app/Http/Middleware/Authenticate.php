<?php

namespace App\Http\Middleware;

use App\Models\Main\Company;
use App\Support\TenantConnector;
use Closure;
use Exception;
use Illuminate\Contracts\Auth\Factory as Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;

class Authenticate
{
    use TenantConnector;
    /**
     * The authentication guard factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory  $auth
     * @return void
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $connect = $this->changeConnection($request);
        if($connect){

            // if ($this->auth->guard($guard)->guest()) {
            //     return response('Unauthorized.', 401);
            // }

            try {
                $apy = JWTAuth::parseToken()->getPayload()->toArray();
            } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
        
                return response()->json(['token_expired'], 404);
        
            } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
        
                return response()->json(['token_invalid'], 401);
        
            } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
        
                return response()->json(['token_absent' => $e->getMessage()], 500);
        
            }



            return $next($request);

        }else{
            return response('Company Not Found',400);
        }

        return response('Internal Server Error.', 500);
        
    }

    public function changeConnection(Request $request) {

        $data = DB::table('companies')->where('name', $request->input('company'))->get();

            if($data->count()>=0){

                $dataConnection = new Company();
                $dataConnection->tenant_host = $data[0]->hostname;
                $dataConnection->tenant_database = $data[0]->database;
                $dataConnection->tenant_username = $data[0]->username;
                $dataConnection->tenant_password = Crypt::decryptString($data[0]->password);
                $this->reconnect($dataConnection);
                
                return true;
            }

        return false;
     
    }
}
