<?php

namespace App\Http\Middleware;

use App\Models\Main\Koperasi;
use App\Support\KoperasiConnector;
use Closure;

class AccessKoperasiMiddleware {

    use KoperasiConnector;

    /**
     * @var Company
     */
    protected $koperasi;

    /**
     * Tenant constructor.
     * @param Koperasi $company
     */
    public function __construct(Koperasi $koperasi) {
        $this->koperasi = $koperasi;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if (($request->session()->get('tenant')) === null)
            return redirect()->route('home')->withErrors(['error' => __('Please select a Koperasi before making this request.')]);

        // Get the company object with the id stored in session
        $koperasi = $this->koperasi->find($request->session()->get('koperasi'));

        // Connect and place the $company object in the view
        $this->reconnect($koperasi);
        $request->session()->put('koperasi', $company);

        return $next($request);
    }
}