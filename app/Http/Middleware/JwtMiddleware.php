<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use App\Models\User;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;

use Illuminate\Http\Request;

use App\Models\Main\Koperasi;

use Illuminate\Support\Facades\Crypt;
use App\Support\KoperasiConnector;
use DB;

use Tymon\JWTAuth\Facades\JWTAuth; 

class JwtMiddleware
{

    use KoperasiConnector;

    public function handle($request, Closure $next, $guard = null)
    {
        $token = $request->bearerToken();

        
        if(!$token) {
            // Unauthorized response if token not there
            return response()->json([
                'error_code' => 401,
                'error_type' => 'token_is_null',
                'error' => 'Token not provided.'
            ], 401);
        }

        try {
            $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);

            if(!$this->changeConnection($request)){
                return response()->json([
                    'error_code' => 401,
                    'error_type' => 'koperasi_not_found',
                    'error' => 'Koperasi not found.'
                ], 401);
            }

            
        } catch(ExpiredException $e) {
            return response()->json([
                'error_code' => 400,
                'error_type' => 'token_expired',
                'error' => 'Provided token is expired.'
            ], 400);
        } catch(Exception $e) {
            return response()->json([
                'error_code' => 400,
                'error_type' => 'token_error',
                'error' => 'An error while decoding token.'
            ], 400);
        }

        $user = User::find($credentials->sub);

        // Now let's put the user in the request class so that you can grab it from there
        $request->auth = $user;
        

        return $next($request);
    }

    public function changeConnection(Request $request) {

        $license_code = $request->header('license_code');
        

        $data = DB::table('TAccess')->where('license_code', $license_code)->get();

        
        // dd( $license_code );
        // dd(Crypt::decryptString(trim($data[0]->password)));
        if($data){
        
            if($data->count()>0){
                
                $dataConnection = new Koperasi();
                $dataConnection->host = trim($data[0]->host);
                $dataConnection->database = trim($data[0]->database);
                $dataConnection->username = trim($data[0]->username);
                $dataConnection->password = Crypt::decryptString(trim($data[0]->password));
                $this->reconnect($dataConnection);
            
                // dd(DB::connection()->getConfig());
                return true;
            }
        }

        return false;
     
    }
}