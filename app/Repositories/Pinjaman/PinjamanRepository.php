<?php

namespace App\Repositories\Pinjaman;


use App\Repositories\Pinjaman\PinjamanInterface as PinjamanInterface;
use App\Models\Pinjaman;
use App\Models\UnitUsaha;

use DB;

use \DateTime;


class PinjamanRepository implements PinjamanInterface
{
    public $pinjaman;


    function __construct(Pinjaman $pinjaman) {
	    $this->pinjaman = $pinjaman;
    }


    public function getAll()
    {
        // return "all";
        return $this->pinjaman->all();
    }

    public function getDataPaginate($val)
    {
        return $this->pinjaman->with('rekeningBank')->toSql();;
    }

    public function getDataSummaryMonthly()
    {

        

        $namasumberdana = DB::select(DB::raw('
            SELECT s.namasumberdana2 namasumberdana,		
            count(distinct m.KdAnggota)NOA,sum(JmlPinjaman)totalpinjaman FROM 		
            TMasterRekeningPinjaman M INNER JOIN TTransaksiRealPinjaman R		
            ON  M.NoRekPinjaman =R.NoRekPinjaman		
            INNER JOIN TProdukPjm P ON p.KdProdukPjm=m.kdprodukpjm		
            INNER JOIN TSumberDana s ON s.KdSumberDana=m.kdprodukpjm		
            where YEAR(R.TglRealisasi)=2020		
            group by s.namasumberdana2		
            '
        ));

        $newdata = [];

        foreach($namasumberdana as $sumberdana) {

            $newdata[] = [
                'name' => $sumberdana->namasumberdana,
                'data' => []
            ];

        }

        
        for($i = 0;$i < count($newdata);$i++) {
            $data = DB::select(DB::raw("
                SELECT YEAR(R.TglRealisasi)TAHUN,MONTH(R.TglRealisasi)bulan,M.kdprodukpjm,s.namasumberdana2 namasumberdana,		
                count(distinct m.KdAnggota)NOA,sum(JmlPinjaman)totalpinjaman FROM 		
                TMasterRekeningPinjaman M INNER JOIN TTransaksiRealPinjaman R		
                ON  M.NoRekPinjaman =R.NoRekPinjaman		
                INNER JOIN TProdukPjm P ON p.KdProdukPjm=m.kdprodukpjm		
                INNER JOIN TSumberDana s ON s.KdSumberDana=m.kdprodukpjm		
                where YEAR(R.TglRealisasi)=2020 AND s.namasumberdana2 = '".$newdata[$i]['name']."'
                group by  YEAR(R.TglRealisasi),MONTH(R.TglRealisasi),M.kdprodukpjm,s.namasumberdana2		
                "
            ));

            foreach($data as $dd){
                // $newdata[$i]['data'][] = $dd;
                $newdata[$i]['data'][0] = $dd->bulan == 1 ? $dd->totalpinjaman : 0;
                $newdata[$i]['data'][1] = $dd->bulan == 2 ? $dd->totalpinjaman : 0;
                $newdata[$i]['data'][2] = $dd->bulan == 3 ? $dd->totalpinjaman : 0;
                $newdata[$i]['data'][3] = $dd->bulan == 4 ? $dd->totalpinjaman : 0;
                $newdata[$i]['data'][4] = $dd->bulan == 5 ? $dd->totalpinjaman : 0;
                $newdata[$i]['data'][5] = $dd->bulan == 6 ? $dd->totalpinjaman : 0;
                $newdata[$i]['data'][6] = $dd->bulan == 7 ? $dd->totalpinjaman : 0;
                $newdata[$i]['data'][7] = $dd->bulan == 8 ? $dd->totalpinjaman : 0;
                $newdata[$i]['data'][8] = $dd->bulan == 9 ? $dd->totalpinjaman : 0;
                $newdata[$i]['data'][9] = $dd->bulan == 10 ? $dd->totalpinjaman : 0;
                $newdata[$i]['data'][10] = $dd->bulan == 11 ? $dd->totalpinjaman : 0;
                $newdata[$i]['data'][11] = $dd->bulan == 12 ? $dd->totalpinjaman : 0;
            }
        }

    

        return $newdata;
    }

    public function getDataSummaryYearly()
    {
        $data = DB::select(DB::raw('
            SELECT YEAR(R.TglRealisasi)TAHUN,M.kdprodukpjm,s.namasumberdana2 namasumberdana,		
            count(distinct m.KdAnggota)NOA,sum(JmlPinjaman)totalpinjaman FROM 		
            TMasterRekeningPinjaman M INNER JOIN TTransaksiRealPinjaman R		
            ON  M.NoRekPinjaman =R.NoRekPinjaman		
            INNER JOIN TProdukPjm P ON p.KdProdukPjm=m.kdprodukpjm		
            INNER JOIN TSumberDana s ON s.KdSumberDana=m.kdprodukpjm		
            group by  s.namasumberdana2,YEAR(R.TglRealisasi),M.kdprodukpjm	        
            '
        ));

        return $data;
    }

    public function getDataSumamry()
    {
        $data = DB::select(DB::raw('
            SELECT s.namasumberdana2 namasumberdana,		
            count(distinct m.KdAnggota)NOA,sum(JmlPinjaman)totalpinjaman FROM 		
            TMasterRekeningPinjaman M INNER JOIN TTransaksiRealPinjaman R		
            ON  M.NoRekPinjaman =R.NoRekPinjaman		
            INNER JOIN TProdukPjm P ON p.KdProdukPjm=m.kdprodukpjm		
            INNER JOIN TSumberDana s ON s.KdSumberDana=m.kdprodukpjm		
            group by  s.namasumberdana2	        
            '
        ));

        return $data;
    }

    public function getDataSummaryaDelinquent()
    {

        $data = DB::select(DB::raw("
            SELECT 
                sum_jt, 
                sum(BakiDebet) BakiDebet, 
                count(KdAnggota) jml_anggota 
            FROM 
                (
                SELECT 
                    r.NoRekPinjaman, 
                    notrxrealpjm, 
                    r.KdAnggota, 
                    r.kdprodukpjm, 
                    p.namaprodukpjm, 
                    r.TglPencairan, 
                    r.JmlAngsuran, 
                    r.TglJatem, 
                    r.JmlPinjaman, 
                    (
                    r.JmlPinjaman - isnull(c.angspokok, 0)
                    ) AS BakiDebet, 
                    case when kol is null then 'L' else kol end kol, 
                    isnull(jmltunggakan, 0) jmltunggakan, 
                    case when isnull(jmltunggakan, 0) = 0 then '1.JT 0 Bulan' when (
                    isnull(jmltunggakan, 0) >= 1 
                    and isnull(jmltunggakan, 0) <= 3
                    ) then '2.JT 1-3 Bulan' when (
                    isnull(jmltunggakan, 0) >= 4 
                    and isnull(jmltunggakan, 0) <= 6
                    ) then '3.JT 4-6 Bulan' when (
                    isnull(jmltunggakan, 0) >= 7 
                    and isnull(jmltunggakan, 0) <= 12
                    ) then '4.JT 7-12 Bulan' when isnull(jmltunggakan, 0) >= 12 then '5.JT >12 Bulan' end Sum_JT, 
                    isnull(tunggakanpokok, 0) tunggakanpokok, 
                    isnull(tunggakanmargin, 0) tunggakanmargin, 
                    ROUND(
                    (r.SukuBungaPerThn / 12), 
                    2
                    ) RATE, 
                    ISNULL(b.angsuran, 0) angsuran, 
                    isnull(sisapinjaman, 0) sisapinjaman 
                from 
                    TMasterRekeningPinjaman r 
                    inner join TProdukPjm p on r.kdprodukpjm = p.kdprodukpjm 
                    inner join TAnggota a on r.KdAnggota = a.KdAnggota 
                    inner join TTransaksiRealPinjaman S on r.norekpinjaman = S.norekpinjaman 
                    left join (
                    SELECT 
                        * 
                    FROM 
                        (
                        select 
                            a.norekpinjaman, 
                            tgljatemp, 
                            angsuran, 
                            Month (tgljatemp) bulan, 
                            year (tgljatemp) tahun 
                        from 
                            TDataAngsuran A 
                            inner join TTransaksiRealPinjaman b on b.norekpinjaman = a.norekpinjaman 
                        WHERE 
                            a.NOURUT <> 0
                        ) a 
                    where 
                        bulan = '12' 
                        and tahun = '2020'
                    ) as b on b.norekpinjaman = r.norekpinjaman 
                    LEFT JOIN (
                    SELECT 
                        norekpinjaman, 
                        sum(angspokok) angspokok, 
                        sum(angsmargin) angsmargin 
                    from 
                        TTransaksiAngsuran 
                    WHERE 
                        TGLBAYAR <= '11/30/2020' 
                    GROUP BY 
                        norekpinjaman
                    ) C ON C.norekpinjaman = r.norekpinjaman 
                    left join (
                    select 
                        norekpinjaman, 
                        case when jmltunggakan <= 3 then 'L' when jmltunggakan <= 6 then 'KL' when jmltunggakan <= 12 then 'DP' when jmltunggakan > 12 then 'M' end kol, 
                        jmltunggakan, 
                        tunggakanmargin, 
                        tunggakanpokok 
                    from 
                        (
                        select 
                            * 
                        from 
                            (
                            select 
                                a.norekpinjaman, 
                                count(nourut) jmltunggakan, 
                                sum(margin)- sum(angsmargin) tunggakanmargin, 
                                sum(pokok)- sum(angspokok) tunggakanpokok 
                            from 
                                (
                                select 
                                    a.norekpinjaman, 
                                    a.nourut, 
                                    pokok, 
                                    flaglunas, 
                                    isnull(margin, 0) margin, 
                                    sum(
                                    isnull(s.angspokok, 0)
                                    ) angspokok, 
                                    sum(
                                    isnull(s.angsmargin, 0)
                                    ) angsmargin, 
                                    pokok - sum(
                                    isnull(s.angspokok, 0)
                                    ) tunggakanpokok, 
                                    (
                                    isnull(margin, 0)- sum(
                                        isnull(s.angsmargin, 0)
                                    )
                                    ) as tunggakanmargin 
                                from 
                                    TDataAngsuran A 
                                    inner join TTransaksiRealPinjaman b on b.norekpinjaman = a.norekpinjaman 
                                    left join TTransaksiAngsuran s on s.norekpinjaman = a.norekpinjaman 
                                    and a.nourut = s.nourut 
                                where 
                                    tgljatemp <= '11/30/2020' 
                                    and flaglunas is null 
                                    and a.nourut <> 0 
                                group by 
                                    a.norekpinjaman, 
                                    a.nourut, 
                                    pokok, 
                                    flaglunas, 
                                    margin
                                ) a 
                            WHERE 
                                (tunggakanpokok + tunggakanmargin)<> 0 
                            group by 
                                a.norekpinjaman
                            ) a 
                        WHERE 
                            (tunggakanpokok + tunggakanmargin)<> 0
                        ) a
                    ) d on d.norekpinjaman = r.norekpinjaman 
                    left join (
                    SELECT 
                        NoRekPinjaman, 
                        (JmlPinjaman - angspokok) sisapinjaman 
                    FROM 
                        (
                        SELECT 
                            p.NoRekPinjaman, 
                            JmlPinjaman, 
                            sum(
                            isnull(angspokok, 0)
                            ) angspokok 
                        FROM 
                            TMasterRekeningPinjaman p 
                            left join TTransaksiRealPinjaman t on p.NoRekPinjaman = t.NoRekPinjaman 
                            LEFT join TTransaksiAngsuran a on a.norekpinjaman = p.norekpinjaman 
                        group by 
                            p.NoRekPinjaman, 
                            JmlPinjaman
                        ) A 
                    where 
                        (JmlPinjaman - angspokok)> 0
                    ) e on e.norekpinjaman = r.norekpinjaman 
                where 
                    r.norekpinjaman is not null 
                    and isnull(sisapinjaman, 0) > 0
                ) A 
            group by 
                sum_jt 
            order by 
                Sum_JT asc
      
        "));
        
        return $data;
            
    }



    public function getDataRealPinjaman($props)
    {
        $series = [];

        $month = date('Y');
        $year = date('m');

    

        if(count($props) > 0) {
            $month = $props['month'];
            $year = $props['year'];
        }

        $name = DB::select(DB::raw('
        SELECT KdProdukPjm,	NamaProdukPjm FROM 		
        TProdukPjm 
            '
        ));

        $categories = [];

        $a_date = date("Y-m-d");
        $day = 31;//date("d", strtotime($a_date));

        foreach($name as $nm){
            $kd = $nm->KdProdukPjm;
            

            

            $num = 0;

            $ser = [
                'name'=>$nm->KdProdukPjm." - ".$nm->NamaProdukPjm,
                'data'=>[],
            ];

            for($i = 0;$i < $day;$i++){
                $categories[$i] = $i + 1;
                $date = $i + 1;
                // $year = 2018;//date('y'); 
                // $month = 7;//date('m');
                $query = DB::select(DB::raw("
                select TglRealisasi,sum(JmlRealisasi) as JmlRealisasi from TTransaksiRealPinjaman where month(TglRealisasi)=$month
                and  year(TglRealisasi)=$year and day(TglRealisasi)=$date and SUBSTRING(NoRekPinjaman, 1, 3) = $kd 
                group by TglRealisasi
                order by day(TglRealisasi)
                "
                ));
                
                if($query){
                    foreach($query as $q){
                        $num = $q->JmlRealisasi + 0;
                        $ser['data'][] = $num;
                    }
                }else{
                    $ser['data'][] = 0;
                }
                
            }

            $series[] = $ser;

        }
        

        return [
            'y-m-d'=> $a_date,
            'today' => $day,
            'categories'=>$categories,
            'series'=>$series
        ];
    }


    public function getDataLaporanKeuanganPusat($tahun_1,$tahun_2)
    {
        if($tahun_1 != null){
            if($tahun_2 != null){
                if($tahun_1 < $tahun_2){
                    return [];
                }
            }else{
                return [];
            }
        }else{
            return [];
        }

        $data  = [];
        $data_tahun2= DB::select(DB::raw("
            select  tahun,data_aset,data_modal,round(((data_modal/data_aset)*100),2) persen_dataaset from 
            (select tahun, nilai data_aset,(select  nilai  from THistoricalPerkiraan  where tahun=$tahun_2  and periode=12
            and ParentAcc='3') data_modal from THistoricalPerkiraan 
            where tahun=$tahun_2  and periode=12
            and ParentAcc='1')a;      
            "
        ));

        $data_tahun1 = DB::select(DB::raw("
            select  tahun,data_aset,data_modal,round(((data_modal/data_aset)*100),2) persen_dataaset from 
            (select $tahun_1 tahun, nilai data_aset,(select  nilai  from TPerkiraan  where 
            ParentAcc='3') data_modal from TPerkiraan 
            where ParentAcc='1')a;   
            "
        ));

        $data = [];

        $unit_usaha = DB::select(DB::raw("
            SELECT [KdKantor]
            ,[NamaKantor]
            FROM [TESTING2].[dbo].[TKantor]  
            "
        ));


        foreach($data_tahun2 as $data2){
            foreach($data_tahun1 as $data1){

                $growth = round(((($data1->data_aset - $data2->data_aset) / $data2->data_aset) * 100), 2, PHP_ROUND_HALF_UP);
                $data = [
                    'product' => 'Laporan Keuangan Pusat',
                    'data_asset' => [
                        $data1->tahun => $data1->data_aset,
                        $data2->tahun => $data2->data_aset,
                        'percent' => [
                            'value' => round($data1->persen_dataaset, 2, PHP_ROUND_HALF_UP),
                            'profit' => round(($data1->persen_dataaset), 2, PHP_ROUND_HALF_UP) > 0 ? true : false
                        ]
                    ],
                    'color' => 'primary',
                    'data_modal'  => [
                        $data1->tahun => [
                            'value' => ($data1->data_modal),
                            'profit' => (($data1->data_modal)) > 0 ? true : false
                        ],
                        $data2->tahun => [
                            'value' => ($data2->data_modal),
                            'profit' => (($data2->data_modal)) > 0 ? true : false
                        ],
                    ],
                    'growth' => [
                        'value' => $growth,
                        'profit' => $growth > 0 ? true : false
                    ],
                    'unit_usaha' =>  $unit_usaha ,
                ];
            }
        }

        $datas = [
            'tahun1' => $data_tahun1,
            'tahun2' => $data_tahun2,
            'data_laporan'=> [$data],
        ];

        return $datas;
    }

    function getDataLaporanKeuanganUnitUsaha($tahun_1,$tahun_2,$tipe_usaha)
    {

        if($tahun_1 != null){
            if($tahun_2 != null){
                if($tahun_1 < $tahun_2){
                    return [];
                }
            }else{
                return [];
            }
        }else{
            return [];
        }

        if($tipe_usaha == null) {
            return [];
        }

        $data  = [];
        $data_tahun2= DB::select(DB::raw("
            select  tahun,data_aset,data_modal,round(((data_modal/data_aset)*100),2) persen_dataaset from 
            (select tahun, nilai data_aset,(select  nilai  from THistoricalPerkiraanul  where tahun=$tahun_2  and periode=12
            and ParentAcc='3'
            and TipeUsaha='02'
            ) data_modal from THistoricalPerkiraanul 
            where tahun=$tahun_2  and periode=12
            and ParentAcc='1'
            and TipeUsaha='$tipe_usaha'
            )a      
            "
        ));

        $data_tahun1 = DB::select(DB::raw("
            select  tahun,data_aset,data_modal,round(((data_modal/data_aset)*100),2) persen_dataaset from 
            (select $tahun_1 tahun, nilai data_aset,(select  nilai  from TPerkiraanul  where 
            ParentAcc='3'
            and TipeUsaha='02'
            ) data_modal from TPerkiraanul 
            where ParentAcc='1'
            and TipeUsaha='$tipe_usaha'
            )a  
            "
        ));

        $data = [];

        $unit_usaha = DB::select(DB::raw("
            SELECT [KdKantor]
            ,[NamaKantor]
            FROM [TESTING2].[dbo].[TKantor]  
            "
        ));

        foreach($data_tahun2 as $data2){
            foreach($data_tahun1 as $data1){

                $growth = round(((($data1->data_aset - $data2->data_aset) / $data2->data_aset) * 100), 2, PHP_ROUND_HALF_UP);

                $data_aset_persen = ($data1->data_modal / $data1->data_aset) * 100;
                $data = [
                    'product' => 'Laporan Keuangan Unit Usaha',
                    'data_asset' => [
                        $data1->tahun => $data1->data_aset,
                        $data2->tahun => $data2->data_aset,
                        'percent' => [
                            'value' => round($data1->persen_dataaset, 2, PHP_ROUND_HALF_UP),
                            'profit' => round(($data1->persen_dataaset), 2, PHP_ROUND_HALF_UP) > 0 ? true : false
                        ]
                    ],
                    'color' => 'primary',
                    'data_modal'  => [
                        $data1->tahun => [
                            'value' => ($data1->data_modal),
                            'profit' => (($data1->data_modal)) > 0 ? true : false
                        ],
                        $data2->tahun => [
                            'value' => ($data2->data_modal),
                            'profit' => (($data2->data_modal)) > 0 ? true : false
                        ],
                    ],
                    'growth' => [
                        'value' => $growth,
                        'profit' => $growth > 0 ? true : false
                    ],
                    'unit_usaha' =>  $unit_usaha ,
                ];
            }
        }

        $datas = [
            'tahun1' => $data_tahun1,
            'tahun2' => $data_tahun2,
            'data_laporan'=> [$data],
        ];

        return $datas;
    }


    public function find($id)
    {
        return $this->pinjaman->findpinjaman($id);
    }


    public function delete($id)
    {
        return $this->pinjaman->deletepinjaman($id);
    }
}