<?php

namespace App\Repositories\Pinjaman;


use Illuminate\Support\ServiceProvider;


class PinjamanRepoServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        
    }


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\Pinjaman\PinjamanInterface', 'App\Repositories\Pinjaman\PinjamanRepository');
    }
}