<?php

namespace App\Repositories\Pinjaman;


interface PinjamanInterface {


    public function getAll();

    
    public function getDataPaginate($val);


    public function getDataSummaryMonthly();

    
    public function getDataSummaryYearly();


    public function getDataSummaryaDelinquent();


    public function getDataLaporanKeuanganPusat($tahun1,$tahun2);


    public function getDataLaporanKeuanganUnitUsaha($tahun1,$tahun2,$unit_usaha);


    public function getDataRealPinjaman($props);


    public function find($id);


    public function delete($id);

    
}