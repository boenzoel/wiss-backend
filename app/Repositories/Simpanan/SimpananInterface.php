<?php

namespace App\Repositories\Simpanan;


interface SimpananInterface {


    public function getAll();

    
    public function getDataPaginate($val);


    public function getDataStatusSummary();


    public function getDataSimpananBerjangkaSummary();


    public function find($id);


    public function delete($id);

    
}