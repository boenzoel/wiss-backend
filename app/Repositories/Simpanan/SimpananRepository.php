<?php

namespace App\Repositories\Simpanan;


use App\Repositories\Simpanan\SimpananInterface as SimpananInterface;
use App\Models\Simpanan;

use DB;

use \DateTime;


class SimpananRepository implements SimpananInterface
{
    public $simpanan;


    function __construct(Simpanan $simpanan) {
	    $this->simpanan = $simpanan;
    }


    public function getAll()
    {
        // return "all";
        return $this->simpanan->all();
    }

    public function getDataPaginate($val)
    {
        return $this->simpanan->with('rekeningBank')->toSql();;
    }

    public function getDataStatusSummary()
    {
        $totalsimpanan = DB::table(DB::raw('TSaldoSimpanan as s'))
            ->select(DB::raw('sum(simpananawal+simpananin-simpananout) as totalsimpanan'))
            ->whereRaw('s.JenisSimpanan=1 and p.flagMobile is null and (simpananawal+simpananin-simpananout)>0')
            ->join('TMasterRekeningSimpanan as m','s.NoRekening','=','m.NoRekening')
            ->join('TProduk as p','p.KdProduk','=','m.KdProduk')
            ->first()->totalsimpanan;

        $data = DB::select(DB::raw('
                    select
                    a.KdProduk,a.NamaProduk,noa,saldosimpanan,'.$totalsimpanan.' as totalsimpanan,
                    (saldosimpanan/'.$totalsimpanan.')*100 persentase
                    from (
                        select a.KdProduk,a.NamaProduk,count(a.NoRekening) as noa,sum(a.saldo) as saldosimpanan 
                        from (
                            select s.NoRekening,m.KdProduk,NamaProduk,KdAnggota,(simpananawal+simpananin-simpananout) as saldo 
                            from TSaldoSimpanan s 
                            inner join TMasterRekeningSimpanan m on s.NoRekening=m.NoRekening
                            inner join TProduk p on p.KdProduk=m.KdProduk
                            
                            where s.JenisSimpanan=1 and p.flagMobile is null
                            and (simpananawal+simpananin-simpananout)>0
                    
                        ) a
                        group by a.KdProduk,a.NamaProduk
                    ) a
                    group by a.KdProduk,a.NamaProduk,noa,a.saldosimpanan
                    order by a.KdProduk
                    '
                    )
                );

        return $data;
    }

    public function getDataSimpananBerjangkaSummary()
    {
        $saldo = DB::table(DB::raw('TSaldoSimpanan as s'))
            ->select(DB::raw('sum(simpananawal+simpananin-simpananout) as saldo'))
            ->whereRaw('s.JenisSimpanan=2')
            ->first()->saldo;

        $data = DB::select(DB::raw('
            select KdProdukSimjaka,NamaProdukSimjaka,noa,saldosimpanan,'.$saldo.' as totalsimpanan,		
            (saldosimpanan/'.$saldo.')*100 persentase		
            from (		
                select KdProdukSimjaka,NamaProdukSimjaka,count(NoRekening)noa,		
                sum(saldo)saldosimpanan 
                from (		
                    select s.NoRekening,m.KdProdukSimjaka,NamaProdukSimjaka,KdAnggota,		
                    (simpananawal+simpananin-simpananout)saldo 
                    from TSaldoSimpanan s 		
                    inner join TMasterRekeningSimjaka m		
                    on s.NoRekening=m.NoRekSimjaka		
                    inner join TProdukSimjaka p on p.KdProdukSimjaka=m.KdProdukSimjaka		
                    where s.JenisSimpanan=2 		
                    and  (simpananawal+simpananin-simpananout)>0		
                )A group by KdProdukSimjaka,NamaProdukSimjaka)A
            '
        ));

        return $data;
    }


    public function find($id)
    {
        return $this->simpanan->findsimpanan($id);
    }


    public function delete($id)
    {
        return $this->simpanan->deletesimpanan($id);
    }
}