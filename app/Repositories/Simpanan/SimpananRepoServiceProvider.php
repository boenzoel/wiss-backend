<?php

namespace App\Repositories\Simpanan;


use Illuminate\Support\ServiceProvider;


class SimpananRepoServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        
    }


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\Simpanan\SimpananInterface', 'App\Repositories\Simpanan\SimpananRepository');
    }
}