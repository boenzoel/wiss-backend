<?php

namespace App\Repositories\UnitUsaha;


interface UnitUsahaInterface {


    public function getAll();

    
    public function getDataPaginate($val);


    public function find($id);


    public function delete($id);

    
}