<?php

namespace App\Repositories\UnitUsaha;


use App\Repositories\UnitUsaha\UnitUsahaInterface as UnitUsahaInterface;
use App\Models\UnitUsaha;

use DB;

use \DateTime;


class UnitUsahaRepository implements UnitUsahaInterface
{
    public $unit_usaha;


    function __construct(UnitUsaha $unit_usaha) {
	    $this->unit_usaha = $unit_usaha;
    }


    public function getAll()
    {
        // return "all";
        return $this->unit_usaha->all();
    }

    public function getDataPaginate($val)
    {
        return $this->unit_usaha->toSql();;
    }


    public function find($id)
    {
        return $this->unit_usaha->findAnggota($id);
    }


    public function delete($id)
    {
        return $this->unit_usaha->deleteAnggota($id);
    }
}