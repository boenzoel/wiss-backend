<?php

namespace App\Repositories\UnitUsaha;


use Illuminate\Support\ServiceProvider;


class UnitUsahaRepoServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        
    }


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\UnitUsaha\UnitUsahaInterface', 'App\Repositories\UnitUsaha\UnitUsahaRepository');
    }
}