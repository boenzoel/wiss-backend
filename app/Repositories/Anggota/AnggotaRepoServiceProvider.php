<?php

namespace App\Repositories\Anggota;


use Illuminate\Support\ServiceProvider;


class AnggotaRepoServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        
    }


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\Anggota\AnggotaInterface', 'App\Repositories\Anggota\AnggotaRepository');
    }
}