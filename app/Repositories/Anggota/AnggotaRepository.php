<?php

namespace App\Repositories\Anggota;


use App\Repositories\Anggota\AnggotaInterface as AnggotaInterface;
use App\Models\Anggota;

use DB;

use \DateTime;


class AnggotaRepository implements AnggotaInterface
{
    public $anggota;


    function __construct(Anggota $anggota) {
	    $this->anggota = $anggota;
    }


    public function getAll()
    {
        // return "all";
        return $this->anggota->all();
    }

    public function getDataPaginate($val)
    {
        return $this->anggota->with('rekeningBank')->toSql();;
    }

    public function getDataSummaryRegisterMonthly()
    {
        $month = $this->anggota->select(DB::raw('DATEPART(Month, TglRegister) as months, DATEPART(Year, TglRegister) as years'))
        ->whereYear('TglRegister', 2020)
        ->groupBy(DB::raw('DATEPART(Month, TglRegister), DATEPART(Year, TglRegister)'))->get();
        
        $monthname = [];
        $total = 0;
        foreach($month as $m){

            $data = $this->anggota->whereRaw("DATEPART(Month, TglRegister) = '$m->months' and DATEPART(Year, TglRegister) = '$m->years'")
            ->groupBy(DB::raw('DATEPART(Month, TglRegister), DATEPART(Year, TglRegister)'))->count();
            $periode =  DateTime::createFromFormat('!m', $m->months);

            $total += $data;

            $monthname[] = [
                'Tahun' => Date('Y'),
                'Periode' => $periode->format('F'),
                'Value' => $data,
            ];
            
        }

        $datas = [
            'data' => $monthname,
            'total' => $total,
        ];

        return $datas;
    }

    public function getDataSummaryRegisterYearly()
    {
        $year = $this->anggota->select(DB::raw('DATEPART(Year, TglRegister) as years'))
        ->whereYear('TglRegister', '!=', Date('Y') )
        ->groupBy(DB::raw('DATEPART(Year, TglRegister)'))->orderBy(DB::raw('DATEPART(Year, TglRegister)'))->get();
        
        $yearname = [];
        $total = 0;
        foreach($year as $y){

            $data = $this->anggota->whereRaw("DATEPART(Year, TglRegister) = '$y->years'")->groupBy(DB::raw('DATEPART(Year, TglRegister)'))->count();

            $total += $data;

            $yearname[] = [
                'Tahun' => $y->years,
                'Value' => $data,
            ];
            
        }

        $datas = [
            'data' => $yearname,
            'total' => $total,
        ];

        return $datas;
    }

    public function getDataListYearSummary()
    {
        
    }


    public function getDataStatusSummary()
    {
        $anggota = $this->anggota->where(['status'=>'Anggota'])->count();
        $calon_anggota = $this->anggota->where(['status'=>'Calon Anggota'])->count();
        $anggota_luarbiasa = $this->anggota->where(['status'=>'Anggota Luar Biasa'])->count();
        $anggota_nonaktif = $this->anggota->where(['status'=>'NonAktif'])->count();

        $total = $anggota + $calon_anggota + $anggota_luarbiasa + $anggota_nonaktif;

        $data = [];

        $data[0] = [
            'name' => 'Anggota',
            'value' => $anggota,
        ];

        $data[1] = [
            'name' => 'Calon Anggota',
            'value' => $calon_anggota,
        ];

        $data[2] = [
            'name' => 'Anggota Luar Biasa',
            'value' => $anggota_luarbiasa,
        ];

        $data[3] = [
            'name' => 'Anggota Non Aktif',
            'value' => $anggota_nonaktif,
        ];

        // $data[4] = [
        //     'name' => 'Total',
        //     'value' => $total,
        // ];
        
        return $data;
    }


    public function find($id)
    {
        return $this->anggota->findAnggota($id);
    }


    public function delete($id)
    {
        return $this->anggota->deleteAnggota($id);
    }
}