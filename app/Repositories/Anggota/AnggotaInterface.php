<?php

namespace App\Repositories\Anggota;


interface AnggotaInterface {


    public function getAll();

    
    public function getDataPaginate($val);


    public function getDataSummaryRegisterMonthly();

    
    public function getDataSummaryRegisterYearly();


    public function getDataStatusSummary();


    public function find($id);


    public function delete($id);

    
}